package co.com.eafit.hrsystem

class Employee {

    static constraints = {
        fullName unique: true, validator: { val, obj ->
            def user = Employee.findByFullNameIlike(val)
            if(user && obj.id != user.id){
                return ['invalid.insensitiveUnique']
            }
        }, nullable: false, blank: false
    }

    String fullName
}
