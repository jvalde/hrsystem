<%@ page import="co.com.eafit.hrsystem.Employee" %>



<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'fullName', 'error')} required">
	<label for="fullName">
		<g:message code="employee.fullName.label" default="Full Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fullName" required="" value="${employeeInstance?.fullName}"/>
</div>

