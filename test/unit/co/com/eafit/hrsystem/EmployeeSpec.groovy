package co.com.eafit.hrsystem

import grails.plugin.spock.UnitSpec
import grails.test.mixin.TestFor

/**
 * Created with IntelliJ IDEA.
 * User: jorgevalderrama
 * Date: 14/03/2013
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
@TestFor(Employee)
class EmployeeSpec extends UnitSpec{   \

    def "probando nombre unico sin case-insensitive"(){

        given: "que tenemos un empleado que se llama Valde"
            new Employee(fullName:"Valde").save()
        when: "cuando intento crear otro empleado que se llama VALDE"
            def employee = new Employee(fullName:"VALDE")
            employee.validate()
        then: "yo espero que el sistema falle"
            assert employee.errors["fullName"].code == "invalid.insensitiveUnique"

    }
}
